<?php

namespace Bobbie\DTrans\Model;

class Sender
{
    public function send($targetDomain, $document, $formatId)
    {
        $servers = dns_get_record("_dtrans_https_$formatId._tcp." . $targetDomain, DNS_SRV);
        $minPri = 65535;
        $targetHost = $targetPort = '';
        foreach ($servers as $server) {
            if ($server['pri'] < $minPri) {
                $targetHost = $server['target'];
                $targetPort = $server['port'];
                $minPri = $server['pri'];
            }
        }
        $url = "https://$targetHost:$targetPort/.well-known/dtrans?format=$formatId";
        //$url = "https://alex14.bobbie.de/.well-known/dtrans?format=$formatId";
        return self::postXMLToServer($url, $document);
    }

    public function sendQBound($document) : string
    {
        $url = 'https://bobbie.api.cleverschein.de/.well-known/dtrans?format=ubl';
        return self::postXMLToServer($url, $document);
    }

    public static function postXMLToServer(string $url, string $data_json)
    {
        return self::postToServer($url,$data_json,'text/xml');
    }

    public static function postToServer(string $url, string $data, string $contentType) : string {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1800);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Content-Type: $contentType"]);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $string = curl_exec($ch);
        if($string === false) {
            $string = curl_error($ch);
        }
        curl_close($ch);
        return $string;
    }

}
