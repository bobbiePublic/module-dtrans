<?php

namespace Bobbie\DTrans\Controller;

use Magento\Framework\App\ActionFactory;
use Magento\Framework\App\ActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Route\ConfigInterface;
use Magento\Framework\App\Router\ActionList;
use Magento\Framework\App\RouterInterface;
use Psr\Log\LoggerInterface;

class Router implements RouterInterface
{
    protected ActionFactory $actionFactory;

    protected ActionList $actionList;

    protected ConfigInterface $routeConfig;

    protected LoggerInterface $logger;

    public function __construct(
        ActionFactory $actionFactory,
        ActionList $actionList,
        ConfigInterface $routeConfig,
        LoggerInterface $logger
    ) {
        $this->actionFactory = $actionFactory;
        $this->actionList = $actionList;
        $this->routeConfig = $routeConfig;
        $this->logger = $logger;
    }

    /**
     * Checks if security.txt file was requested and returns instance of matched application action class
     *
     * @param RequestInterface $request
     * @return ActionInterface|null
     */
    public function match(RequestInterface $request)
    {
        $identifier = trim($request->getPathInfo(), '/');

        if ($identifier === '.well-known/dtrans') {
            $modules = $this->routeConfig->getModulesByFrontName('dtrans');
            switch (mb_strtolower($request->getParam('format'))) {
                case 'ubl':
                case '1lieferschein':
                    $actionClassName = $this->actionList->get($modules[0], null, 'receiver', 'UBL');
                    break;
                case 'unece':
                    $actionClassName = $this->actionList->get($modules[0], null, 'receiver', 'unece');
                    break;
                default:
                    $this->logger->warn('unkown DTRANS Format: ' . $request->getParam('format'));
                    return null;
            }
            return $this->actionFactory->create($actionClassName);
        } else {
            return null;
        }
    }
}
