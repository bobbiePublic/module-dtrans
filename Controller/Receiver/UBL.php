<?php

namespace Bobbie\DTrans\Controller\Receiver;

use Bobbie\UBL\Helper\Serializer;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;

class UBL extends Action implements HttpPostActionInterface, HttpGetActionInterface, CsrfAwareActionInterface
{
    private ResultFactory $resultPageFactory;

    protected Serializer $serializer;


    public function __construct(
        Context $context,
        ResultFactory $resultPageFactory,
        Serializer $serializer
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->serializer = $serializer;
        parent::__construct($context);
    }

    public function execute()
    {
        $data = file_get_contents('php://input');
        $object = $this->serializer->deSerializeXML($data);

        $result = $this->resultPageFactory->create(ResultFactory::TYPE_RAW);
        $result->setHeader('Content-Type', 'text/plain');
        $result->setContents(print_r( $object ,true));
        return $result;
    }

    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}
